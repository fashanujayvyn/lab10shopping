/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author jayvy
 */
public class ShoppingCartDemo {
    
    public static void main (String args []){
        PaymentServiceFactory factory = PaymentServiceFactory.getInstance();
        PaymentService creditService = factory.getPaymentService(PaymentServiceType.CREDIT);
        PaymentService debitService = factory.getPaymentService(PaymentServiceType.DEBIT);
        
        Cart cart = new Cart();
        cart.addProduct( new Product("shirt", 50));
        cart.addProduct(new Product("pants" , 60));
        
        cart.setPaymentService(debitService);
        cart.payCart();
        
        cart.setPaymentService(creditService);
        cart.payCart();
    }
    
}
